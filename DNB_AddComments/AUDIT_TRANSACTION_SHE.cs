//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class AUDIT_TRANSACTION_SHE
    {
        public long AUDIT_TRANSACTION_ID { get; set; }
        public Nullable<System.DateTime> TIMESTAMP { get; set; }
        public Nullable<long> SESSION_ID { get; set; }
        public string TRANSACTION_NAME { get; set; }
        public string TRANSACTION_REASON { get; set; }
    
        public virtual LIMS_SESSION LIMS_SESSION { get; set; }
    }
}
