//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class ENTITY_PICTURE
    {
        public long SCHEMA_ENTITY_ID { get; set; }
        public string PICTURE_ITEM_STATUS { get; set; }
        public string SELECTED_ICON { get; set; }
        public string UNSELECTED_ICON { get; set; }
    
        public virtual SCHEMA_ENTITY SCHEMA_ENTITY { get; set; }
    }
}
