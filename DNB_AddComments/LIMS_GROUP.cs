//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class LIMS_GROUP
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LIMS_GROUP()
        {
            this.ALIQUOT = new HashSet<ALIQUOT>();
            this.ALIQUOT_SHE = new HashSet<ALIQUOT_SHE>();
            this.ALIQUOT_TEMPLATE = new HashSet<ALIQUOT_TEMPLATE>();
            this.ANALYTE = new HashSet<ANALYTE>();
            this.AQC_CHART = new HashSet<AQC_CHART>();
            this.AQC_RULE = new HashSet<AQC_RULE>();
            this.CALCULATION = new HashSet<CALCULATION>();
            this.CALCULATION_CONSTANT = new HashSet<CALCULATION_CONSTANT>();
            this.CALENDAR_EVENT_TYPE = new HashSet<CALENDAR_EVENT_TYPE>();
            this.CHEMICAL = new HashSet<CHEMICAL>();
            this.CHERRYPICK_TEMPLATE = new HashSet<CHERRYPICK_TEMPLATE>();
            this.CLIENT = new HashSet<CLIENT>();
            this.COFA = new HashSet<COFA>();
            this.COFA_TEMPLATE = new HashSet<COFA_TEMPLATE>();
            this.CONTAINER_TYPE = new HashSet<CONTAINER_TYPE>();
            this.DESTINATION = new HashSet<DESTINATION>();
            this.FOLDER = new HashSet<FOLDER>();
            this.GRADE = new HashSet<GRADE>();
            this.HAZARD = new HashSet<HAZARD>();
            this.INSPECTION_PLAN = new HashSet<INSPECTION_PLAN>();
            this.INSTRUMENT = new HashSet<INSTRUMENT>();
            this.INSTRUMENT_GROUP = new HashSet<INSTRUMENT_GROUP>();
            this.INSTRUMENT_TYPE = new HashSet<INSTRUMENT_TYPE>();
            this.U_INTERNAL_ORDER = new HashSet<U_INTERNAL_ORDER>();
            this.U_LOC_TYPE_ENV = new HashSet<U_LOC_TYPE_ENV>();
            this.U_PICKLIST = new HashSet<U_PICKLIST>();
            this.PLATE = new HashSet<PLATE>();
            this.PLATE_TEMPLATE = new HashSet<PLATE_TEMPLATE>();
            this.U_REQUEST_ALIQUOT = new HashSet<U_REQUEST_ALIQUOT>();
            this.U_SAMPLE_HISTORY = new HashSet<U_SAMPLE_HISTORY>();
            this.SIGNATURE_TEMPLATE = new HashSet<SIGNATURE_TEMPLATE>();
            this.U_SSI_REQUESTS = new HashSet<U_SSI_REQUESTS>();
            this.STOCK_TYPE = new HashSet<STOCK_TYPE>();
            this.LOCATION = new HashSet<LOCATION>();
            this.LOCATION_TYPE = new HashSet<LOCATION_TYPE>();
            this.OPERATOR_GROUP = new HashSet<OPERATOR_GROUP>();
            this.PHRASE_HEADER = new HashSet<PHRASE_HEADER>();
            this.PLATE_PLAN = new HashSet<PLATE_PLAN>();
            this.PLATE_PLAN_SHE = new HashSet<PLATE_PLAN_SHE>();
            this.PLATE_PLAN_TEMPLATE = new HashSet<PLATE_PLAN_TEMPLATE>();
            this.PLATE_SHE = new HashSet<PLATE_SHE>();
            this.PRODUCT = new HashSet<PRODUCT>();
            this.PROJECT = new HashSet<PROJECT>();
            this.RACK = new HashSet<RACK>();
            this.RACK_SHE = new HashSet<RACK_SHE>();
            this.RACK_TYPE = new HashSet<RACK_TYPE>();
            this.REPORT = new HashSet<REPORT>();
            this.RESULT_TEMPLATE = new HashSet<RESULT_TEMPLATE>();
            this.SAMPLE = new HashSet<SAMPLE>();
            this.SAMPLE_SHE = new HashSet<SAMPLE_SHE>();
            this.SAMPLE_TEMPLATE = new HashSet<SAMPLE_TEMPLATE>();
            this.SCRIPT = new HashSet<SCRIPT>();
            this.SDG = new HashSet<SDG>();
            this.SDG_SHE = new HashSet<SDG_SHE>();
            this.SDG_TEMPLATE = new HashSet<SDG_TEMPLATE>();
            this.SPECIFICATION = new HashSet<SPECIFICATION>();
            this.STOCK_TEMPLATE = new HashSet<STOCK_TEMPLATE>();
            this.STUDY = new HashSet<STUDY>();
            this.STUDY_SHE = new HashSet<STUDY_SHE>();
            this.STUDY_TEMPLATE = new HashSet<STUDY_TEMPLATE>();
            this.SUPPLIER = new HashSet<SUPPLIER>();
            this.SYNTAX = new HashSet<SYNTAX>();
            this.TEST = new HashSet<TEST>();
            this.TEST_SHE = new HashSet<TEST_SHE>();
            this.TEST_TEMPLATE = new HashSet<TEST_TEMPLATE>();
            this.UNIT = new HashSet<UNIT>();
            this.WORKSHEET_ALIQUOT_TYPE = new HashSet<WORKSHEET_ALIQUOT_TYPE>();
            this.WORKFLOW = new HashSet<WORKFLOW>();
            this.WORKSHEET_FORMAT = new HashSet<WORKSHEET_FORMAT>();
            this.WORKSHEET = new HashSet<WORKSHEET>();
            this.WORKSHEET_SHE = new HashSet<WORKSHEET_SHE>();
            this.WORKSHEET_TEMPLATE = new HashSet<WORKSHEET_TEMPLATE>();
            this.WORKSTATION = new HashSet<WORKSTATION>();
            this.WORKSTATION_GROUP = new HashSet<WORKSTATION_GROUP>();
            this.XML_TRANSACTION = new HashSet<XML_TRANSACTION>();
        }
    
        public long GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION_STATUS { get; set; }
        public string SYSTEM_DATA { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALIQUOT> ALIQUOT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALIQUOT_SHE> ALIQUOT_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALIQUOT_TEMPLATE> ALIQUOT_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ANALYTE> ANALYTE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_CHART> AQC_CHART { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_RULE> AQC_RULE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CALCULATION> CALCULATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CALCULATION_CONSTANT> CALCULATION_CONSTANT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CALENDAR_EVENT_TYPE> CALENDAR_EVENT_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CHEMICAL> CHEMICAL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CHERRYPICK_TEMPLATE> CHERRYPICK_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CLIENT> CLIENT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COFA> COFA { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COFA_TEMPLATE> COFA_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTAINER_TYPE> CONTAINER_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DESTINATION> DESTINATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FOLDER> FOLDER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GRADE> GRADE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HAZARD> HAZARD { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSPECTION_PLAN> INSPECTION_PLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT> INSTRUMENT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT_GROUP> INSTRUMENT_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT_TYPE> INSTRUMENT_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<U_INTERNAL_ORDER> U_INTERNAL_ORDER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<U_LOC_TYPE_ENV> U_LOC_TYPE_ENV { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<U_PICKLIST> U_PICKLIST { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE> PLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_TEMPLATE> PLATE_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<U_REQUEST_ALIQUOT> U_REQUEST_ALIQUOT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<U_SAMPLE_HISTORY> U_SAMPLE_HISTORY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SIGNATURE_TEMPLATE> SIGNATURE_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<U_SSI_REQUESTS> U_SSI_REQUESTS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STOCK_TYPE> STOCK_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LOCATION> LOCATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LOCATION_TYPE> LOCATION_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OPERATOR_GROUP> OPERATOR_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PHRASE_HEADER> PHRASE_HEADER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN> PLATE_PLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN_SHE> PLATE_PLAN_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN_TEMPLATE> PLATE_PLAN_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_SHE> PLATE_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROJECT> PROJECT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RACK> RACK { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RACK_SHE> RACK_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RACK_TYPE> RACK_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REPORT> REPORT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RESULT_TEMPLATE> RESULT_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAMPLE> SAMPLE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAMPLE_SHE> SAMPLE_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAMPLE_TEMPLATE> SAMPLE_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SCRIPT> SCRIPT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SDG> SDG { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SDG_SHE> SDG_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SDG_TEMPLATE> SDG_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SPECIFICATION> SPECIFICATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STOCK_TEMPLATE> STOCK_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDY> STUDY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDY_SHE> STUDY_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STUDY_TEMPLATE> STUDY_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SUPPLIER> SUPPLIER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SYNTAX> SYNTAX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEST> TEST { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEST_SHE> TEST_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEST_TEMPLATE> TEST_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UNIT> UNIT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSHEET_ALIQUOT_TYPE> WORKSHEET_ALIQUOT_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKFLOW> WORKFLOW { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSHEET_FORMAT> WORKSHEET_FORMAT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSHEET> WORKSHEET { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSHEET_SHE> WORKSHEET_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSHEET_TEMPLATE> WORKSHEET_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSTATION> WORKSTATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSTATION_GROUP> WORKSTATION_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<XML_TRANSACTION> XML_TRANSACTION { get; set; }
    }
}
