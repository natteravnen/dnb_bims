//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class SCHEMA_PROMPT_ORDER
    {
        public long SCHEMA_ENTITY_ID { get; set; }
        public decimal PROMPT_ORDER_NUMBER { get; set; }
        public Nullable<long> SCHEMA_FIELD_PROMPT_ID { get; set; }
    
        public virtual SCHEMA_ENTITY SCHEMA_ENTITY { get; set; }
        public virtual SCHEMA_FIELD_PROMPT SCHEMA_FIELD_PROMPT { get; set; }
    }
}
