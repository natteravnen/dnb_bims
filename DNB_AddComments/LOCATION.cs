//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class LOCATION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LOCATION()
        {
            this.ALIQUOT = new HashSet<ALIQUOT>();
            this.ALIQUOT_SHE = new HashSet<ALIQUOT_SHE>();
            this.INSTRUMENT = new HashSet<INSTRUMENT>();
            this.PLATE = new HashSet<PLATE>();
            this.SDG_USER = new HashSet<SDG_USER>();
            this.LOCATION1 = new HashSet<LOCATION>();
            this.LOCATION11 = new HashSet<LOCATION>();
            this.PLATE_PLAN = new HashSet<PLATE_PLAN>();
            this.PLATE_PLAN_SHE = new HashSet<PLATE_PLAN_SHE>();
            this.PLATE_SHE = new HashSet<PLATE_SHE>();
            this.RACK_LOCATION = new HashSet<RACK_LOCATION>();
            this.RACK_LOCATION_SHE = new HashSet<RACK_LOCATION_SHE>();
            this.SAMPLE = new HashSet<SAMPLE>();
            this.SAMPLE_SHE = new HashSet<SAMPLE_SHE>();
            this.SDG_USER_SHE = new HashSet<SDG_USER_SHE>();
            this.STOCK_TEMPLATE = new HashSet<STOCK_TEMPLATE>();
            this.TEST = new HashSet<TEST>();
            this.TEST_SHE = new HashSet<TEST_SHE>();
            this.TEST_TEMPLATE = new HashSet<TEST_TEMPLATE>();
            this.WORKSTATION = new HashSet<WORKSTATION>();
            this.HAZARD = new HashSet<HAZARD>();
        }
    
        public long LOCATION_ID { get; set; }
        public Nullable<long> PARENT_ID { get; set; }
        public string GRID_REFERENCE { get; set; }
        public string ELEVATION { get; set; }
        public string CAN_STORE_ITEMS { get; set; }
        public Nullable<long> GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION_STATUS { get; set; }
        public Nullable<long> LOCATION_TYPE_ID { get; set; }
        public Nullable<long> PARENT_VERSION_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALIQUOT> ALIQUOT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALIQUOT_SHE> ALIQUOT_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT> INSTRUMENT { get; set; }
        public virtual LIMS_GROUP LIMS_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE> PLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SDG_USER> SDG_USER { get; set; }
        public virtual LOCATION_TYPE LOCATION_TYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LOCATION> LOCATION1 { get; set; }
        public virtual LOCATION LOCATION2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LOCATION> LOCATION11 { get; set; }
        public virtual LOCATION LOCATION3 { get; set; }
        public virtual LOCATION_USER LOCATION_USER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN> PLATE_PLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN_SHE> PLATE_PLAN_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_SHE> PLATE_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RACK_LOCATION> RACK_LOCATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RACK_LOCATION_SHE> RACK_LOCATION_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAMPLE> SAMPLE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAMPLE_SHE> SAMPLE_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SDG_USER_SHE> SDG_USER_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<STOCK_TEMPLATE> STOCK_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEST> TEST { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEST_SHE> TEST_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEST_TEMPLATE> TEST_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSTATION> WORKSTATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HAZARD> HAZARD { get; set; }
    }
}
