//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class U_SAMPLE_HISTORY_USER
    {
        public long U_SAMPLE_HISTORY_ID { get; set; }
        public Nullable<System.DateTime> U_DATE_TIME { get; set; }
        public string U_LOG { get; set; }
        public string U_ENTITY_ID { get; set; }
        public string U_OPERATOR_ID { get; set; }
        public string U_TYPE { get; set; }
        public string U_ENTITY_NAME { get; set; }
    
        public virtual U_SAMPLE_HISTORY U_SAMPLE_HISTORY { get; set; }
    }
}
