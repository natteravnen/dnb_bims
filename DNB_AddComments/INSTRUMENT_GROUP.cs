//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class INSTRUMENT_GROUP
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INSTRUMENT_GROUP()
        {
            this.INSTRUMENT_GROUP1 = new HashSet<INSTRUMENT_GROUP>();
            this.TEST_TEMPLATE = new HashSet<TEST_TEMPLATE>();
            this.WORKSHEET_TEMPLATE_SESSION = new HashSet<WORKSHEET_TEMPLATE_SESSION>();
            this.INSTRUMENT = new HashSet<INSTRUMENT>();
        }
    
        public long INSTRUMENT_GROUP_ID { get; set; }
        public Nullable<long> GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION_STATUS { get; set; }
        public Nullable<long> PARENT_VERSION_ID { get; set; }
    
        public virtual LIMS_GROUP LIMS_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT_GROUP> INSTRUMENT_GROUP1 { get; set; }
        public virtual INSTRUMENT_GROUP INSTRUMENT_GROUP2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEST_TEMPLATE> TEST_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKSHEET_TEMPLATE_SESSION> WORKSHEET_TEMPLATE_SESSION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT> INSTRUMENT { get; set; }
    }
}
