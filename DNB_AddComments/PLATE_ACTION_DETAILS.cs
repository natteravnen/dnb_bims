//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class PLATE_ACTION_DETAILS
    {
        public long PLATE_ACTION_ID { get; set; }
        public long PLATE_ID { get; set; }
        public Nullable<long> PLATE_TEMPLATE_ID { get; set; }
        public string PARAMETER_1 { get; set; }
        public string PARAMETER_2 { get; set; }
        public string PARAMETER_3 { get; set; }
        public string PARAMETER_4 { get; set; }
        public string PARAMETER_5 { get; set; }
    
        public virtual PLATE PLATE { get; set; }
        public virtual PLATE_ACTION PLATE_ACTION { get; set; }
        public virtual PLATE_TEMPLATE PLATE_TEMPLATE { get; set; }
    }
}
