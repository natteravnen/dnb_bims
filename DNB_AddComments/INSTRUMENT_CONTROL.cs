//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class INSTRUMENT_CONTROL
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INSTRUMENT_CONTROL()
        {
            this.INSTRUMENT = new HashSet<INSTRUMENT>();
            this.INSTRUMENT_TYPE = new HashSet<INSTRUMENT_TYPE>();
        }
    
        public long INSTRUMENT_CONTROL_ID { get; set; }
        public Nullable<long> BUILDING_SCRIPT_ID { get; set; }
        public Nullable<long> PARSING_SCRIPT_ID { get; set; }
        public Nullable<long> MAPPING_SCRIPT_ID { get; set; }
        public string INTERFACE_TYPE { get; set; }
        public string INTERFACE_NAME { get; set; }
        public string BAUD_RATE { get; set; }
        public string DATA_BITS { get; set; }
        public string STOP_BITS { get; set; }
        public string PARITY { get; set; }
        public string FLOW_CONTROL { get; set; }
        public string INPUT_FILE_DIRECTORY { get; set; }
        public string INPUT_FILE_EXTENSION { get; set; }
        public string OUTPUT_FILE_DIRECTORY { get; set; }
        public string OUTPUT_FILE_EXTENSION { get; set; }
        public string COPY_FILE { get; set; }
        public string PRINT_FILE { get; set; }
        public string FILE_DELIMITING { get; set; }
        public string START_OF_FILE { get; set; }
        public string END_OF_FILE { get; set; }
        public string MESSAGE_DELIMITING { get; set; }
        public string START_OF_MESSAGE { get; set; }
        public string END_OF_MESSAGE { get; set; }
        public Nullable<decimal> SKIP_AFTER_SOM { get; set; }
        public Nullable<decimal> TIMEOUT { get; set; }
        public string DISCARD_CHARS { get; set; }
        public string START_COMMAND { get; set; }
        public string REPLY_EOM { get; set; }
        public string REPLY_EOF { get; set; }
        public Nullable<long> WORKSTATION_ID { get; set; }
        public Nullable<long> WORKSHEET_ID { get; set; }
        public string INPUT_FILE_SUBDIRECTORIES { get; set; }
        public string RSF_DIRECTORY { get; set; }
        public string RSF_EXTENSION { get; set; }
        public Nullable<long> WORKSHEET_FORMAT_ID { get; set; }
        public string RSF_PREVIEW { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT> INSTRUMENT { get; set; }
        public virtual SCRIPT SCRIPT { get; set; }
        public virtual SCRIPT SCRIPT1 { get; set; }
        public virtual SCRIPT SCRIPT2 { get; set; }
        public virtual WORKSHEET_FORMAT WORKSHEET_FORMAT { get; set; }
        public virtual WORKSTATION WORKSTATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INSTRUMENT_TYPE> INSTRUMENT_TYPE { get; set; }
        public virtual INSTRUMENT_PARAMETERS INSTRUMENT_PARAMETERS { get; set; }
        public virtual WORKSHEET WORKSHEET { get; set; }
    }
}
