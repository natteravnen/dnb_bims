﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DNB_AddComments
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DBQuery myDBQ = new DBQuery();

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = myDBQ;
            SetupApp();
        }

        private void SetupApp()
        {
            ChangeStatusOnUpdateDatabaseButton(false);
            //this.lblStatus.Text = "Loaded";

        }

        private void ListBox_Drop(object sender, DragEventArgs e)
        {
            List<string> newData = (List<string>)e.Data;
        }

        private void ListBox_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void lstSource_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
           
        }
        private void StartProgressBar(int MaxTimeInSeconds, int step)
        {

            //new Thread(() =>
            //{
            //    pgbProgress.Dispatcher.BeginInvoke(new Action(() =>
            //    {
            //        pgbProgress.Value = 0;
            //        pgbProgress.Maximum = MaxTimeInSeconds;
            //    }));

            //    DateTime start = DateTime.Now;
            //    while ((DateTime.Now - start) < new TimeSpan(0, 0, MaxTimeInSeconds))
            //    {

            //        pgbProgress.Dispatcher.BeginInvoke(new Action(() =>
            //        {
            //            pgbProgress.Value = (DateTime.Now - start).TotalSeconds;

            //        }));
            //        Thread.Sleep(1000);
            //    }

            //    //    Thread.CurrentThread.IsBackground = true;
            //    //    pgbProgress.Value = 0;
            //    //    pgbProgress.Maximum = MaxTimeInSeconds;
            //    //    //Thread.Sleep(100);
            //    //    DateTime start = DateTime.Now;
            //    //    while ((DateTime.Now - start) < new TimeSpan(0, 0, MaxTimeInSeconds))
            //    //    {
            //    //        pgbProgress.Value = (DateTime.Now - start).TotalSeconds;

            //    //        Thread.Sleep(1000);
            //    //    }
            //}).Start();
            //Thread.Sleep(100);
        }
        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            //StartProgressBar(120, 10);
            ChangeStatusOnUpdateDatabaseButton(false);
            if (!myDBQ.FillGrid(ref dgResult, lstSource, cboTarget, (bool) chkWithChildren.IsChecked))
                MessageBox.Show("Error when filling grid with data" + myDBQ.LastError);
            HideUnwantedColumns();
            Mouse.OverrideCursor = null;
        }

        private void btnGo_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            if (!myDBQ.AddCommentInGrid(ref dgResult, txtComment.Text))
                MessageBox.Show("Der skete en fejl da tabellen skulle opdateres: " + myDBQ.LastError);
            else
                ChangeStatusOnUpdateDatabaseButton(true);
            Mouse.OverrideCursor = null;
        }

        private void btnUpdateDb_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            if (!myDBQ.UpdateAliquotTableWithComment(ref dgResult))
                MessageBox.Show("Der skete en fejl da databasen skulle opdateres" + myDBQ.LastError);
            ChangeStatusOnUpdateDatabaseButton(false);
            Mouse.OverrideCursor = null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/DNB_AddComments;component/Resources/MainPic.jpg")));
        }

        private void MenuItemSlet_Click(object sender, RoutedEventArgs e)
        {
            if (lstSource.SelectedIndex == -1)
                return;

            lstSource.Items.RemoveAt(lstSource.SelectedIndex);
        }

        private void MenuItemSletAlt_Click(object sender, RoutedEventArgs e)
        {
            lstSource.Items.Clear();
        }
        private void MenuItemRet_Click(object sender, RoutedEventArgs e)
        {

        }
        private void MenuItemPaste_Click(object sender, RoutedEventArgs e)
        {
            PasteFormClipboardToListbox(lstSource);
        }

        private void ChangeStatusOnUpdateDatabaseButton(bool Enable)
        {
            btnUpdateDb.IsEnabled = Enable;
            if (Enable)
            {
                btnUpdateDb.Background = Brushes.Green;
                btnUpdateDb.Foreground = Brushes.White;
            }
            else
            {
                btnUpdateDb.Background = Brushes.Gray;
                btnUpdateDb.Foreground = Brushes.LightGray;
            }
        }
        private void PasteFormClipboardToListbox(ListBox myListB)
        {
            myListB.Items.Clear();
        
            foreach (var i in myDBQ.CopyFromClipboard())
            {
                if (i.Trim() != string.Empty)
                    myListB.Items.Add(i.Trim());
            }
        }

        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if(txtInput.Text.Trim()!=string.Empty)
                lstSource.Items.Add(txtInput.Text);
            }
        }

        private void btnConvertToExcel_Click(object sender, RoutedEventArgs e)
        {
            ExcelTools.ConvertToExcel(dgResult);
        }

        private void dgResult_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            //dgResult.CanUserSortColumns = false;
            //foreach (DataGridColumn column in dgResult.Columns)
            //{
            //    column.CanUserSort = false;
            //}
        }

        private void cboTarget_LostFocus(object sender, RoutedEventArgs e)
        {
            string target = ((ComboBoxItem)cboTarget.SelectedItem).Content.ToString();

            if (target == "Aliquot stregkode")
            {
                chkWithChildren.IsEnabled = true;
            }
            else
            {
                chkWithChildren.IsEnabled = false;
            }
        }

        private void btnDisposeAll_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            if (!myDBQ.AddDisposeInGrid(ref dgResult, true))
                MessageBox.Show("Der skete en fejl da tabellen skulle opdateres" + myDBQ.LastError);
            else
                ChangeStatusOnUpdateDatabaseButton(true);
            Mouse.OverrideCursor = null;
        }

        private void cboStandardText_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox myB = (sender as ComboBox);
            if (myB.SelectedItem != null)
            {
                string text = (myB.SelectedItem as ComboBoxItem).Content as string;
                // If there allready is a comment, add space and new text else just new text.
                txtComment.Text += txtComment.Text.Length > 0 ? " " + text : text;
                myB.Text = "-- Standard Tekst --";
                myB.UpdateLayout();
            }
        }

        private void cboStandardText_LostFocus(object sender, RoutedEventArgs e)
        {
            //(sender as ComboBox).Text = "-- Standard Tekst --";
        }

        private void cboStandardText_DropDownClosed(object sender, EventArgs e)
        {
            (sender as ComboBox).Text = "-- Standard Tekst --";
        }

        private void chkAddUserNameInComment_Checked(object sender, RoutedEventArgs e)
        {
            myDBQ.AddUserNameToComment = (bool)((CheckBox)sender).IsChecked;
        }

        private void chkAddUserNameInComment_Unchecked(object sender, RoutedEventArgs e)
        {
            myDBQ.AddUserNameToComment = (bool)((CheckBox)sender).IsChecked;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            //BackgroundWorker worker = new BackgroundWorker();
            //worker.WorkerReportsProgress = true;
            //worker.DoWork += worker_DoWork;
            //worker.ProgressChanged += Worker_ProgressChanged;
            //worker.RunWorkerAsync();
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgbProgress.Value = e.ProgressPercentage;
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(100);
            }
        }

        private void dgResult_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void HideUnwantedColumns()
        {
            foreach (var c in dgResult.Columns)
            {
                if (c.Header.ToString() == "Plate_ID")
                    c.Visibility = Visibility.Hidden;
                switch (c.Header.ToString().ToUpper())
                {
                    case "ALIQUOT ID":
                    case "PLADE ID":
                        c.Visibility = Visibility.Hidden;
                        break;
                    default:
                        break;
                }

            }
        }
    }
}
