//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONFIG_ITEM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONFIG_ITEM()
        {
            this.COMMAND = new HashSet<COMMAND>();
            this.CONFIG_OPERATOR = new HashSet<CONFIG_OPERATOR>();
            this.CONFIG_ROLE = new HashSet<CONFIG_ROLE>();
            this.MENU = new HashSet<MENU>();
            this.PROMPT_DEFINITION = new HashSet<PROMPT_DEFINITION>();
        }
    
        public long CONFIG_ITEM_ID { get; set; }
        public string USER_MODIFIABLE { get; set; }
        public Nullable<long> CONFIG_GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string SYSTEM_VALUE { get; set; }
        public string SYSTEM_ONLY { get; set; }
        public string TYPE_PARAM { get; set; }
        public string CONFIG_DATA_TYPE { get; set; }
        public string CONFIG_ITEM_TYPE { get; set; }
        public string DESCRIPTION { get; set; }
        public string PROMPT_OBJECT_NAME { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COMMAND> COMMAND { get; set; }
        public virtual CONFIG_GROUP CONFIG_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONFIG_OPERATOR> CONFIG_OPERATOR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONFIG_ROLE> CONFIG_ROLE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MENU> MENU { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROMPT_DEFINITION> PROMPT_DEFINITION { get; set; }
    }
}
