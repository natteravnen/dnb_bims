//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class ALIQUOT_USER
    {
        public long ALIQUOT_ID { get; set; }
        public string U_APPROVED { get; set; }
        public string U_CHECKED_OUT { get; set; }
        public Nullable<long> U_CHECKED_OUT_BY { get; set; }
        public Nullable<System.DateTime> U_CHECKED_OUT_ON { get; set; }
        public Nullable<long> U_CLIENT_ID { get; set; }
        public string U_DISPOSED { get; set; }
        public string U_IN_USE { get; set; }
        public string U_INCLUDE { get; set; }
        public string U_PRE_BARCODED { get; set; }
        public string U_LOCKED { get; set; }
        public string U_SHIPMENT { get; set; }
        public Nullable<long> U_LM_FILTER_ID { get; set; }
        public string U_PATIENT_ID_NO { get; set; }
        public Nullable<System.DateTime> U_COLLECTION_DATE { get; set; }
        public Nullable<System.DateTime> U_RECEIVE_DATE { get; set; }
        public string U_SPECIMEN_COMMENT { get; set; }
        public string U_REQUESTED_ANALYSIS { get; set; }
        public string U_SLABWAREID { get; set; }
        public string U_SPOSITIONID { get; set; }
        public Nullable<System.DateTime> U_ACTIONDATETIME { get; set; }
        public string U_TRACKBC { get; set; }
        public string U_POSITION { get; set; }
        public Nullable<decimal> U_DNB_ORDER_NR { get; set; }
        public string U_SAMPLED_YEAR { get; set; }
        public Nullable<decimal> U_KONC { get; set; }
        public string U_LOG_ROBOT { get; set; }
        public string U_PROJECTNR { get; set; }
        public string U_MOTHER_NAME { get; set; }
        public string U_CPR { get; set; }
        public Nullable<System.DateTime> U_BIRTH_DAY { get; set; }
        public string U_GENDER { get; set; }
        public Nullable<decimal> U_WEIGHT { get; set; }
        public string U_CHILD_CPR { get; set; }
        public string U_PARENT_CPR { get; set; }
        public string U_CHILD_CPR_2 { get; set; }
        public string U_VARMEBEHANDLET { get; set; }
        public string U_BSG_ID { get; set; }
        public string U_PIPETTING_COMMENT { get; set; }
        public string U_HEMOLYSED { get; set; }
        public Nullable<decimal> U_AMOUNT_USED { get; set; }
        public string U_KIT_LOT { get; set; }
    
        public virtual ALIQUOT ALIQUOT { get; set; }
        public virtual OPERATOR OPERATOR { get; set; }
        public virtual CLIENT CLIENT { get; set; }
        public virtual FILTER FILTER { get; set; }
    }
}
