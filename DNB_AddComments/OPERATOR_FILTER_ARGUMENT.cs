//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class OPERATOR_FILTER_ARGUMENT
    {
        public long OPERATOR_FILTER_ARGUMENT_ID { get; set; }
        public Nullable<long> FILTER_ARGUMENT_ID { get; set; }
        public Nullable<long> OPERATOR_ID { get; set; }
        public Nullable<decimal> VALUE_NUMBER { get; set; }
        public string VALUE_TEXT { get; set; }
        public Nullable<System.DateTime> VALUE_DATE { get; set; }
    
        public virtual FILTER_ARGUMENT FILTER_ARGUMENT { get; set; }
        public virtual OPERATOR OPERATOR { get; set; }
    }
}
