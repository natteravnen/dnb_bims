//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class AQC_CHART
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AQC_CHART()
        {
            this.AQC_BLOCK = new HashSet<AQC_BLOCK>();
            this.AQC_CHART1 = new HashSet<AQC_CHART>();
            this.AQC_CHART_RULE = new HashSet<AQC_CHART_RULE>();
        }
    
        public long AQC_CHART_ID { get; set; }
        public Nullable<long> PARENT_VERSION_ID { get; set; }
        public Nullable<long> WORKSHEET_TEMPLATE_ID { get; set; }
        public Nullable<long> WORKSHEET_ALIQUOT_TYPE_ID { get; set; }
        public Nullable<long> RESULT_TEMPLATE_ID { get; set; }
        public Nullable<long> INSTRUMENT_ID { get; set; }
        public Nullable<long> GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION_STATUS { get; set; }
        public Nullable<decimal> TARGET { get; set; }
        public Nullable<decimal> PRECISION { get; set; }
        public Nullable<decimal> BIAS { get; set; }
        public string CHART_TYPE { get; set; }
        public Nullable<decimal> WARNING_LIMIT { get; set; }
        public Nullable<decimal> ACTION_LIMIT { get; set; }
        public Nullable<long> SUBGROUP_SIZE { get; set; }
        public Nullable<long> BLOCK_SIZE { get; set; }
        public string LIMIT_METHOD { get; set; }
        public Nullable<long> LIMIT_PARAMETER { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_BLOCK> AQC_BLOCK { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_CHART> AQC_CHART1 { get; set; }
        public virtual AQC_CHART AQC_CHART2 { get; set; }
        public virtual INSTRUMENT INSTRUMENT { get; set; }
        public virtual LIMS_GROUP LIMS_GROUP { get; set; }
        public virtual RESULT_TEMPLATE RESULT_TEMPLATE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_CHART_RULE> AQC_CHART_RULE { get; set; }
        public virtual WORKSHEET_ALIQUOT_TYPE WORKSHEET_ALIQUOT_TYPE { get; set; }
        public virtual WORKSHEET_TEMPLATE WORKSHEET_TEMPLATE { get; set; }
    }
}
