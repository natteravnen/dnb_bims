//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class SPECIFICATION_ITEM_TYPE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SPECIFICATION_ITEM_TYPE()
        {
            this.SPECIFICATION_ITEM = new HashSet<SPECIFICATION_ITEM>();
        }
    
        public long SPECIFICATION_ITEM_TYPE_ID { get; set; }
        public string NAME { get; set; }
        public string OBJECT_NAME { get; set; }
        public string PROMPT_OBJECT_NAME { get; set; }
        public string RESULT_TYPES { get; set; }
        public string PHRASE_REQUIRED { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SPECIFICATION_ITEM> SPECIFICATION_ITEM { get; set; }
    }
}
