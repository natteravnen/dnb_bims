﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DNB_AddComments
{
    public class DBQuery:INotifyPropertyChanged
    {
        DBAccess myDB = new DBAccess();

        public DBQuery()
        {

        }

        #region Properties

        private double _ProgressValue = 0;
        public double ProgressValue { get { return _ProgressValue; } set { _ProgressValue = value; NotifyPropertyChanged("ProgressValue"); } }

        private int _ProgressMax = 100;
        public int ProgressMax { get { return _ProgressMax; } set { _ProgressMax = value; NotifyPropertyChanged("ProgressMax"); } }

        private string _LastError = string.Empty;
        public string LastError { get { return _LastError; } set { _LastError = value; NotifyPropertyChanged("LastError"); } }

        private bool _AddUserNameToComment = true;

        public bool AddUserNameToComment { get { return _AddUserNameToComment; } set { _AddUserNameToComment = value; NotifyPropertyChanged("AddUserNameToComment"); } }

        private string _Status = string.Empty;

        public string Status { get { return _Status; } set { _Status = value; NotifyPropertyChanged("Status"); } }

        private string _Info = string.Empty;

        public string Info { get { return _Info; } set { _Info = value; NotifyPropertyChanged("Info"); } }

        #endregion Properties

        public bool FillGrid(ref DataGrid myGrid, ListBox myList, ComboBox cboTarget, bool withChildren)
        {
            // TODO Move to mainwindow.xaml.cs
            try
            {
                //StartProgressBar(120, 10);
                // Values from listbox
                List<string> findUs = new List<string>();
                foreach (var i in myList.Items)
                {
                    findUs.Add(i.ToString());
                }

                Status = string.Format("Searchlist: {0}", findUs.Count());
                // Target table
                ComboBoxItem cbo = (ComboBoxItem)cboTarget.SelectedItem;
                string target = cbo.Content.ToString();
                switch (target)
                {
                    case "Aliquot stregkode":
                        myGrid.ItemsSource = myDB.GetAliquotsFromListOfExternalRef(findUs, withChildren);
                        break;
                    case "Aliquot ID":
                        myGrid.ItemsSource = myDB.GetAliquotListFromIDs(findUs);
                        break;
                    case "Plade stregkode":
                        myGrid.ItemsSource = myDB.GetAliquotsFromListOfPlateIDs(findUs);
                        break;
                    case "Sample":
                        break;
                    default:
                        break;
                }
                Info = myGrid.Items.Count.ToString();
                return true;
            }
            catch (Exception ex)
            {
                _LastError = ex.Message;
                return false;
            }
        }

        public bool AddCommentInGrid(ref DataGrid myGrid, string Comment)
        {
            try
            {
                ProgressMax = myGrid.Items.Count;
                if (_AddUserNameToComment && !IsUsernameInComment(Comment))
                {
                    Comment += string.Format(" - {0} : {1}", WindowsIdentity.GetCurrent().Name, DateTime.Now.ToString("dd-MM-yyyy HH:mm"));
                }
                int a = 0;
                foreach (var i in myGrid.Items)
                {
                    try
                    {
                        ProgressValue = a++;
                        AliInfo x = new AliInfo();
                        x = (AliInfo)i;
                        if ((x.Storage !=null) && (x.Storage.Trim().Length > 1))
                        {
                            x.Storage += " - " + Comment;
                        }
                        else
                        {
                            x.Storage = Comment;
                        }
                    }
                    catch (Exception)
                    {
                        // Last entry in grid will be caught here
                    }
                }
                myGrid.Items.Refresh();
                return true;
            }
            catch (Exception ex)
            {
                _LastError = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Updates each Aliquot's storage field (Comment)
        /// so the user can choose individual comments 
        /// on each aliquot
        /// </summary>
        /// <param name="myGrid"></param>
        /// <returns></returns>
        public bool UpdateAliquotTableWithComment(ref DataGrid myGrid)
        {
            try
            {
                List<long> myIDs = new List<long>();
                string comment = string.Empty;
                
                foreach (var i in myGrid.Items)
                {
                    try
                    {
                        if (i.GetType() == typeof(AliInfo))
                        {
                            AliInfo x = (AliInfo)i;
                            if (x != null)
                            {
                                if (!myDB.UpdateAliquotsWithNewCommentsAndDisposeIfNeeded(x))
                                    return false;
                            }
                            
                        }
                    }
                    catch (Exception e)
                    {
                        _LastError = e.Message;
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                _LastError = ex.Message;
                return false;
            }
        }

        public bool AddDisposeInGrid(ref DataGrid myGrid, bool disposeMe)
        {
            try
            {
                foreach (var i in myGrid.Items)
                {
                    try
                    {
                        AliInfo x = (AliInfo)i;
                        x.Disposed = disposeMe;
                    }
                    catch (Exception e)
                    { }
                }
                myGrid.Items.Refresh();
                return true;
            }
            catch (Exception ex)
            {
                _LastError = ex.Message;
                return false;
            }
        }

        public bool IsUsernameInComment(string comment)
        {
            try
            {
                string userName = WindowsIdentity.GetCurrent().Name;
                return comment.Contains(userName);
            }
            catch (Exception ex)
            {
                _LastError = ex.Message;
                return false;
            }
        }

        public string[] CopyFromClipboard()
        {
            string text = Clipboard.GetText(TextDataFormat.Text);
            string[] txt;
            if (text.Contains("\n"))
            {
                text = text.Replace('\r', ' ');
                text = text.Replace('\t', ' ');
                txt = text.Split('\n');
            }
            else if (text.Contains("','"))
            {
                text = text.Replace("'", string.Empty);
                txt = text.Split(',');
            }
            else if (text.Contains(","))
            {
                txt = text.Split(',');
            }
            else
            {
                txt = text.Split(' ');
            }
            return txt;
        }

        private void StartProgressBar(int MaxTimeInSeconds, int step)
        {
            //Thread myProg = new Thread(()=>
            //{
            //    Thread.CurrentThread.IsBackground = true;
            //    ProgressValue = 0;
            //    ProgressMax = MaxTimeInSeconds;
            //    //Thread.Sleep(100);
            //    DateTime start = DateTime.Now;
            //    while ((DateTime.Now - start) < new TimeSpan(0, 0, MaxTimeInSeconds))
            //    {
            //        ProgressValue = (DateTime.Now - start).TotalSeconds;
                    
            //        Thread.Sleep(1000);
            //    }

            //    //for (int i = step; i < MaxTimeInSeconds; i+=step)
            //    //{
            //    //    ProgressValue = i;
            //    //    Thread.Sleep(1000);
            //    //}
                

            //});
            //myProg.Start();
            //Thread.Sleep(100);
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        #endregion PropertyChanged
    }
}
