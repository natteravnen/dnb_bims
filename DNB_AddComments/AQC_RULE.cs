//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class AQC_RULE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AQC_RULE()
        {
            this.AQC_CHART_RULE = new HashSet<AQC_CHART_RULE>();
            this.AQC_FAILURE = new HashSet<AQC_FAILURE>();
            this.AQC_RULE1 = new HashSet<AQC_RULE>();
        }
    
        public long AQC_RULE_ID { get; set; }
        public Nullable<long> PARENT_VERSION_ID { get; set; }
        public Nullable<long> GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION_STATUS { get; set; }
        public string RULE_TYPE { get; set; }
        public string FAILURE_TYPE { get; set; }
        public Nullable<long> POINT_COUNT { get; set; }
        public string LIMIT_TYPE { get; set; }
        public Nullable<decimal> LIMIT_FACTOR { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_CHART_RULE> AQC_CHART_RULE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_FAILURE> AQC_FAILURE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AQC_RULE> AQC_RULE1 { get; set; }
        public virtual AQC_RULE AQC_RULE2 { get; set; }
        public virtual LIMS_GROUP LIMS_GROUP { get; set; }
    }
}
