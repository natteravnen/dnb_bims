//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class AQC_RESULT
    {
        public long AQC_POINT_ID { get; set; }
        public long RESULT_ID { get; set; }
        public long WORKSHEET_ENTRY_ID { get; set; }
    
        public virtual AQC_POINT AQC_POINT { get; set; }
        public virtual RESULT RESULT { get; set; }
        public virtual WORKSHEET_ENTRY WORKSHEET_ENTRY { get; set; }
    }
}
