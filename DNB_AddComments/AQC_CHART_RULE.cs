//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class AQC_CHART_RULE
    {
        public long AQC_CHART_ID { get; set; }
        public long AQC_RULE_ID { get; set; }
        public long RULE_NUMBER { get; set; }
    
        public virtual AQC_CHART AQC_CHART { get; set; }
        public virtual AQC_RULE AQC_RULE { get; set; }
    }
}
