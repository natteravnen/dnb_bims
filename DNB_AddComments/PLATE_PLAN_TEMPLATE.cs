//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class PLATE_PLAN_TEMPLATE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PLATE_PLAN_TEMPLATE()
        {
            this.PLATE_PLAN = new HashSet<PLATE_PLAN>();
            this.PLATE_PLAN_SHE = new HashSet<PLATE_PLAN_SHE>();
            this.PLATE_PLAN_TEMPLATE_FIELD = new HashSet<PLATE_PLAN_TEMPLATE_FIELD>();
            this.PLATE_PLAN_TEMPLATE1 = new HashSet<PLATE_PLAN_TEMPLATE>();
        }
    
        public long PLATE_PLAN_TEMPLATE_ID { get; set; }
        public Nullable<long> PARENT_VERSION_ID { get; set; }
        public Nullable<long> SYNTAX_ID { get; set; }
        public Nullable<long> GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION_STATUS { get; set; }
        public string STATUS { get; set; }
    
        public virtual LIMS_GROUP LIMS_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN> PLATE_PLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN_SHE> PLATE_PLAN_SHE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN_TEMPLATE_FIELD> PLATE_PLAN_TEMPLATE_FIELD { get; set; }
        public virtual SYNTAX SYNTAX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLATE_PLAN_TEMPLATE> PLATE_PLAN_TEMPLATE1 { get; set; }
        public virtual PLATE_PLAN_TEMPLATE PLATE_PLAN_TEMPLATE2 { get; set; }
    }
}
