//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class INSTRUMENT_CONFIG
    {
        public long INSTRUMENT_ID { get; set; }
        public long INSTRUMENT_PART_ID { get; set; }
        public System.DateTime ASSEMBLED_ON { get; set; }
        public Nullable<System.DateTime> DISASSEMBLED_ON { get; set; }
        public Nullable<long> ASSEMBLED_BY { get; set; }
        public Nullable<long> DISASSEMBLED_BY { get; set; }
    
        public virtual INSTRUMENT INSTRUMENT { get; set; }
        public virtual INSTRUMENT INSTRUMENT1 { get; set; }
        public virtual LIMS_SESSION LIMS_SESSION { get; set; }
        public virtual LIMS_SESSION LIMS_SESSION1 { get; set; }
    }
}
