//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class PBCATCOL
    {
        public string PBC_TNAM { get; set; }
        public Nullable<decimal> PBC_TID { get; set; }
        public string PBC_OWNR { get; set; }
        public string PBC_CNAM { get; set; }
        public Nullable<decimal> PBC_CID { get; set; }
        public string PBC_LABL { get; set; }
        public Nullable<decimal> PBC_LPOS { get; set; }
        public string PBC_HDR { get; set; }
        public Nullable<decimal> PBC_HPOS { get; set; }
        public Nullable<decimal> PBC_JTFY { get; set; }
        public string PBC_MASK { get; set; }
        public Nullable<decimal> PBC_CASE { get; set; }
        public Nullable<decimal> PBC_HGHT { get; set; }
        public Nullable<decimal> PBC_WDTH { get; set; }
        public string PBC_PTRN { get; set; }
        public string PBC_BMAP { get; set; }
        public string PBC_INIT { get; set; }
        public string PBC_CMNT { get; set; }
        public string PBC_EDIT { get; set; }
        public string PBC_TAG { get; set; }
    }
}
