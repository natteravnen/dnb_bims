//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class GRADE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GRADE()
        {
            this.GRADE_ACTION = new HashSet<GRADE_ACTION>();
            this.GRADE1 = new HashSet<GRADE>();
            this.SPECIFICATION_GRADE = new HashSet<SPECIFICATION_GRADE>();
        }
    
        public long GRADE_ID { get; set; }
        public Nullable<long> GROUP_ID { get; set; }
        public string NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION_STATUS { get; set; }
        public Nullable<long> PARENT_VERSION_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GRADE_ACTION> GRADE_ACTION { get; set; }
        public virtual LIMS_GROUP LIMS_GROUP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GRADE> GRADE1 { get; set; }
        public virtual GRADE GRADE2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SPECIFICATION_GRADE> SPECIFICATION_GRADE { get; set; }
    }
}
