//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class FILTER_ARGUMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FILTER_ARGUMENT()
        {
            this.OPERATOR_FILTER_ARGUMENT = new HashSet<OPERATOR_FILTER_ARGUMENT>();
        }
    
        public long FILTER_ARGUMENT_ID { get; set; }
        public Nullable<long> FILTER_ID { get; set; }
        public decimal ORDER_NUMBER { get; set; }
        public string ARGUMENT_TYPE { get; set; }
        public Nullable<decimal> VALUE_NUMBER { get; set; }
        public string VALUE_TEXT { get; set; }
        public Nullable<System.DateTime> VALUE_DATE { get; set; }
        public Nullable<long> SCHEMA_ENTITY_ID { get; set; }
        public string PROMPT_FOR_VALUE { get; set; }
        public Nullable<long> PHRASE_ID { get; set; }
        public string NAME { get; set; }
    
        public virtual FILTER FILTER { get; set; }
        public virtual PHRASE_HEADER PHRASE_HEADER { get; set; }
        public virtual SCHEMA_ENTITY SCHEMA_ENTITY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OPERATOR_FILTER_ARGUMENT> OPERATOR_FILTER_ARGUMENT { get; set; }
    }
}
