//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class CALCULATION_ARGUMENT
    {
        public long CALCULATION_ARGUMENT_ID { get; set; }
        public Nullable<long> CALCULATION_ID { get; set; }
        public string NAME { get; set; }
        public Nullable<decimal> ORDER_NUMBER { get; set; }
        public string CALCULATION_VALUE_TYPE { get; set; }
    
        public virtual CALCULATION CALCULATION { get; set; }
    }
}
