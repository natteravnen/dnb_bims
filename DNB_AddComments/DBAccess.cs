﻿using DNBTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DNB_AddComments
{
    public class DBAccess
    {
        NautEntities myDB = new NautEntities();
        public DBAccess()
        {

        }

        public List<AliInfo> GetAliquotsFromListOfExternalRef(List<string> myList, bool withChildren)
        {
            try
            {
                List<AliInfo> myRes = new List<AliInfo>();

                if (myList.Count == 1)
                {
                    myRes = GetAliquotListWithLikeQuery(myList);
                
                }
                else
                {
                    myRes = withChildren ? GetAliquotFromExternalRefWithChildren(myList) : GetAliquotFromExternalRefWithoutChildren(myList);
                }
                return myRes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fejl ved opslag: {0} ", ex.Message));
                SysLog.Log(string.Format("GetAliquotsFromListOfExternalRef: {0}", ex.Message), LogLevel.Error);
                return null;
            }
        }

        public List<AliInfo> GetAliquotsFromListOfPlateIDs(List<string> myList)
        {
            try
            {
                List<long> queryPlateIDs = (from p in myDB.PLATE
                                            where myList.Contains(p.EXTERNAL_REFERENCE)
                                            select p.PLATE_ID
                                            ).ToList<long>();


                var queryPlate = (from p in myDB.ALIQUOT

                                  join pb in myDB.PLATE
                                  on p.PLATE_ID equals pb.PLATE_ID

                                  where queryPlateIDs.Contains((long)p.PLATE_ID)
                                  select new AliInfo
                                  {
                                      Aliquot_ID = p.ALIQUOT_ID,
                                      Name = p.NAME,
                                      External_Reference = p.EXTERNAL_REFERENCE,
                                      Storage = p.STORAGE,
                                      Plate_ID = (long)p.PLATE_ID,
                                      MasterBC = " - ",
                                      PlateBC = pb.EXTERNAL_REFERENCE,
                                      CreatedOn = (DateTime)p.CREATED_ON
                                  }
                             );


                return queryPlate.ToList();
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("GetAliquotsFromListOfPlateIDs: {0}", ex.Message), LogLevel.Error);
                return null;
            }
        }

        public bool UpdateAliquotsWithNewCommentsAndDisposeIfNeeded(AliInfo myInfo)
        {
            try
            {
                ALIQUOT myAliquot = myDB.ALIQUOT.FirstOrDefault(p => p.ALIQUOT_ID == myInfo.Aliquot_ID);
                if (myAliquot != null)
                {
                    myAliquot.STORAGE = myInfo.Storage;
                    ALIQUOT_USER myDisposedAliquots = myDB.ALIQUOT_USER.FirstOrDefault(d => d.ALIQUOT_ID == myInfo.Aliquot_ID);

                    // Dispose or not
                    if (myAliquot.CONDITION == "U" && myAliquot.LOCATION_ID == 5948 && myDisposedAliquots.U_DISPOSED == "T" && myInfo.Disposed == false)
                    {
                        // Un-dispose
                        myDisposedAliquots.U_DISPOSED = null;

                        // We need to find the correct location for the aliquot by using the one from the plate
                        if (myInfo.Plate_ID == null)
                        {
                            myAliquot.LOCATION_ID = null;
                        }
                        else
                        {
                            PLATE myPlate = myDB.PLATE.FirstOrDefault(p => p.PLATE_ID == myInfo.Plate_ID);
                            if (myPlate != null)
                            {
                                myAliquot.LOCATION_ID = myPlate.LOCATION_ID;
                            }
                        }
                        myAliquot.CONDITION = null;
                    }
                    if (myInfo.Disposed && myAliquot.LOCATION_ID != 5948 && myDisposedAliquots.U_DISPOSED != "T")
                    {
                        myAliquot.LOCATION_ID = 5948;
                        myAliquot.CONDITION = "U";
                        myDisposedAliquots.U_DISPOSED = "T";
                    }
                    myDB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("UpdateAliquotsWithNewCommentsAndDisposeIfNeeded: {0}", ex.Message), LogLevel.Error);
                return false;
            }
            return true;
        }

        public bool UpdateAliquotsWithSameNewComment(List<long> myIDs, string comment, bool disposeMe)
        {
            try
            {

                var query = (from p in myDB.ALIQUOT
                             where myIDs.Contains(p.ALIQUOT_ID)
                             select p
                     );

                List<ALIQUOT> myAliquots = new List<ALIQUOT>();

                myAliquots = query.ToList();
                foreach (ALIQUOT a in myAliquots)
                {
                    a.STORAGE = comment;
                    if (disposeMe)
                    {
                        ALIQUOT_USER myDisposedAliquots = myDB.ALIQUOT_USER.FirstOrDefault(d => d.ALIQUOT_ID == a.ALIQUOT_ID);
                        myDisposedAliquots.U_DISPOSED = "T";
                    }
                }
                myDB.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                SysLog.Log(string.Format("UpdateAliquotsWithSameNewComment: {0}", ex.Message), LogLevel.Error);
                return false;
            }
        }
        private List<AliInfo> GetAliquotFromExternalRefWithChildren(List<string> myList)
        {
            List<AliInfo> myRes = new List<AliInfo>();
            try
            {
                // Find master aliquot's
                var q1 = (from p in myDB.ALIQUOT

                          join au in myDB.ALIQUOT_USER
                          on p.ALIQUOT_ID equals au.ALIQUOT_ID

                          where myList.Contains(p.EXTERNAL_REFERENCE)
                          select new AliInfo
                          {
                              Aliquot_ID = p.ALIQUOT_ID,
                              Name = p.NAME,
                              External_Reference = p.EXTERNAL_REFERENCE,
                              Storage = p.STORAGE,
                              Plate_ID = (long)p.PLATE_ID,
                              Disposed = au.U_DISPOSED == "T" ? true : false
                          }
                         );

                List<AliInfo> lstMaster = q1.ToList();
                
                // Collect master aliquot id in list so we can use it in the next query
                List<long> lstAID = new List<long>();
                foreach (var i in lstMaster)
                {
                    if(i!=null)
                        lstAID.Add(i.Aliquot_ID);
                }

                var query = (from p in myDB.ALIQUOT

                             join af in myDB.ALIQUOT_FORMULATION
                             on p.ALIQUOT_ID equals af.CHILD_ALIQUOT_ID

                             join am in myDB.ALIQUOT
                             on af.PARENT_ALIQUOT_ID equals am.ALIQUOT_ID

                             join pb in myDB.PLATE
                             on p.PLATE_ID equals pb.PLATE_ID

                             join au in myDB.ALIQUOT_USER
                             on am.ALIQUOT_ID equals au.ALIQUOT_ID

                             where lstAID.Contains(af.PARENT_ALIQUOT_ID)

                             select new AliInfo
                             {
                                 Aliquot_ID = af.CHILD_ALIQUOT_ID,
                                 Name = p.NAME,
                                 External_Reference = p.EXTERNAL_REFERENCE,
                                 Storage = p.STORAGE,
                                 Plate_ID = (long)p.PLATE_ID,
                                 MasterBC = am.EXTERNAL_REFERENCE,
                                 PlateBC = pb.EXTERNAL_REFERENCE,
                                 CreatedOn = (DateTime)p.CREATED_ON,
                                 Disposed = au.U_DISPOSED == "T" ? true : false
                             }
                            );
              
                myRes = query.OrderBy(x=>x.PlateBC).ToList();
                myRes.AddRange(lstMaster);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fejl ved 'GetAliquotFromExternalRefWithChildren' opslag: {0} - {1} ", ex.Message, ex.InnerException));
                SysLog.Log(string.Format("GetAliquotFromExternalRefWithChildren: {0}", ex.Message), LogLevel.Error);
                return null;
            }
            return myRes;
        }

        private List<AliInfo> GetAliquotFromExternalRefWithoutChildren(List<string> myList)
        {
            List<AliInfo> myRes = new List<AliInfo>();
            try
            {
                var query = (from p in myDB.ALIQUOT

                             join au in myDB.ALIQUOT_USER
                             on p.ALIQUOT_ID equals au.ALIQUOT_ID

                             where myList.Contains(p.EXTERNAL_REFERENCE)

                             select new AliInfo
                             {
                                 Aliquot_ID = p.ALIQUOT_ID,
                                 Name = p.NAME,
                                 External_Reference = p.EXTERNAL_REFERENCE,
                                 Storage = p.STORAGE,
                                 Plate_ID = (long)p.PLATE_ID,
                                 CreatedOn = (DateTime)p.CREATED_ON,
                                 Disposed = au.U_DISPOSED == "T" ? true : false
                             }
                            );
                myRes = query.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
            return myRes;
        }

        private List<AliInfo> GetAliquotListWithLikeQuery(List<string> myList)
        {
            try
            {
                string search = myList[0];
                if (search.EndsWith("%"))
                    search = search.Substring(0, search.Length - 1);
                var query = (from p in myDB.ALIQUOT

                             join au in myDB.ALIQUOT_USER
                             on p.ALIQUOT_ID equals au.ALIQUOT_ID

                             where p.EXTERNAL_REFERENCE.StartsWith(search)
                             select new AliInfo
                             {
                                 Aliquot_ID = p.ALIQUOT_ID,
                                 Name = p.NAME,
                                 External_Reference = p.EXTERNAL_REFERENCE,
                                 Storage = p.STORAGE,
                                 Plate_ID = (long)p.PLATE_ID,
                                 Disposed = au.U_DISPOSED == "T" ? true : false
                             }
                );
                return query.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fejl ved 'like' opslag: {0} - {1} ", ex.Message, ex.InnerException));
                return null;
            }
        }

        public List<AliInfo> GetAliquotListFromIDs(List<string> myList)
        {
            try
            {
                List<AliInfo> myRes = new List<AliInfo>();

                myRes = GetAliquotFromIDsWithoutChildren(myList);

                return myRes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fejl ved opslag: {0} ", ex.Message));
                SysLog.Log(string.Format("GetAliquotsFromListOfExternalRef: {0}", ex.Message), LogLevel.Error);
                return null;
            }
        }

        private List<AliInfo> GetAliquotFromIDsWithoutChildren(List<string> myList)
        {
            List<AliInfo> myRes = new List<AliInfo>();
            try
            {
                // Collect master aliquot id in list so we can use it in the next query
                List<long> lstAID = new List<long>();
                foreach (var i in myList)
                {
                    if (i != null)
                        lstAID.Add(Convert.ToInt64(i));
                }

                var query = (from p in myDB.ALIQUOT

                             join au in myDB.ALIQUOT_USER
                             on p.ALIQUOT_ID equals au.ALIQUOT_ID

                             where lstAID.Contains(p.ALIQUOT_ID)

                             select new AliInfo
                             {
                                 Aliquot_ID = p.ALIQUOT_ID,
                                 Name = p.NAME,
                                 External_Reference = p.EXTERNAL_REFERENCE,
                                 Storage = p.STORAGE,
                                 Plate_ID = (long)p.PLATE_ID,
                                 CreatedOn = (DateTime)p.CREATED_ON,
                                 Disposed = au.U_DISPOSED == "T" ? true : false
                             }
                           );
                myRes = query.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
            return myRes;
        }
    }
}
