﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNB_AddComments
{
    public class AliInfo
    {
        [Display(Name="Aliquot ID")]
        public long Aliquot_ID { get; set; }

        [Display(Name = "Navn")]
        public string Name { get; set; }

        [Display(Name ="Stregkode")]
        public string External_Reference { get; set; }

        [Display(Name="Kommentar")]
        public string Storage { get; set; }

        [Display(Name ="Plade ID")]
        public long? Plate_ID { get; set; }

        [Display(Name = "Master stregkode")]
        public string MasterBC { get; set; }

        [Display(Name ="Plade stregkode")]
        public string PlateBC { get; set; }

        [Display(Name ="Oprettet den")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Disposed")]
        public bool Disposed { get; set; }

        public AliInfo()
        {
            CreatedOn = DateTime.MinValue;
        }
    }
}
