//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DNB_AddComments
{
    using System;
    using System.Collections.Generic;
    
    public partial class FOLDER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FOLDER()
        {
            this.FOLDER1 = new HashSet<FOLDER>();
            this.FOLDER_SHORTCUT = new HashSet<FOLDER_SHORTCUT>();
        }
    
        public long FOLDER_ID { get; set; }
        public Nullable<long> PARENT_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string NAME { get; set; }
        public Nullable<long> SCHEMA_ENTITY_ID { get; set; }
        public Nullable<long> GROUP_ID { get; set; }
        public Nullable<long> OPERATOR_ID { get; set; }
        public string OPERATOR_ACCESS_LEVEL { get; set; }
        public string GROUP_ACCESS_LEVEL { get; set; }
        public string STANDARD_ACCESS_LEVEL { get; set; }
        public Nullable<long> FILTER_ID { get; set; }
        public Nullable<long> REPORT_ID { get; set; }
        public string ALLOW_CREATE { get; set; }
        public Nullable<int> REFRESH_TIME { get; set; }
        public string INHERIT_FILTER { get; set; }
    
        public virtual FILTER FILTER { get; set; }
        public virtual LIMS_GROUP LIMS_GROUP { get; set; }
        public virtual SCHEMA_ENTITY SCHEMA_ENTITY { get; set; }
        public virtual OPERATOR OPERATOR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FOLDER> FOLDER1 { get; set; }
        public virtual FOLDER FOLDER2 { get; set; }
        public virtual REPORT REPORT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FOLDER_SHORTCUT> FOLDER_SHORTCUT { get; set; }
    }
}
